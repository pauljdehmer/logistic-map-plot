import "typeface-roboto";

import CssBaseline from "@material-ui/core/CssBaseline";
import Layout from "../Layout/Layout";
import { Provider } from "react-redux";
import React from "react";
import { ThemeProvider } from "@material-ui/core/styles";
import configureStore from "../../redux/store";
import theme from "./theme";
import { withStyles } from "@material-ui/core/styles";

const styles = theme => ({
  "@global": {
    "*::-webkit-scrollbar": {
      width: ".3rem"
    },
    "*::-webkit-scrollbar-thumb": {
      background: "#666"
    }
  }
});

function App() {
  return (
    <Provider store={configureStore()}>
      <ThemeProvider theme={theme}>
        <CssBaseline>
          <Layout />
        </CssBaseline>
      </ThemeProvider>
    </Provider>
  );
}

export default withStyles(styles, { name: "ScrollbarBaseline" })(App);
