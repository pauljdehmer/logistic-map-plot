import App from "../App/App";
import React from "react";
import { render } from "@testing-library/react";

test("renders test", () => {
  const { getByRole } = render(<App />);
  const element = getByRole("button");
  expect(element).toHaveTextContent(/settings/i);
});
