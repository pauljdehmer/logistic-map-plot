import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { useEffect } from "react";
import { useRef } from "react";
import { useSelector } from "react-redux";
import { useState } from "react";

const useStyles = makeStyles(theme => ({
  canvasContainer: {
    backgroundColor: "#333",
    height: "100%",
    left: 0,
    position: "fixed",
    top: 0,
    width: "100%"
  }
}));

function Canvas() {
  const classes = useStyles();
  const canvasContainer = useRef(null);
  const canvas = useRef(null);
  const equationValues = useSelector(state => state.equationValues);
  const [clientDimensions, setClientDimensions] = useState([0, 0]);

  useEffect(() => {
    let width, height, devicePixelRatio;

    const getClientDimensions = () => {
      devicePixelRatio = window.devicePixelRatio;
      width = canvasContainer.current.clientWidth * devicePixelRatio;
      height = canvasContainer.current.clientHeight * devicePixelRatio;
      return [width, height];
    };

    const handleResize = () => {
      setClientDimensions(getClientDimensions());
    };

    handleResize();
    window.addEventListener("resize", handleResize);
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  useEffect(() => {
    let ctx,
      lambda,
      xValues,
      x,
      y,
      iterations,
      lambdaLoopsBeforeCanvasUpdate,
      rectangleSize;

    ctx = canvas.current.getContext("2d");

    const draw = () => {
      lambda = equationValues.lambdaStart;
      equationValues.lambdaStep =
        (equationValues.lambdaEnd - equationValues.lambdaStart) /
        (ctx.canvas.width * equationValues.superSamples);
      rectangleSize = 0.5;

      clearCanvas();
      drawGrid();
      startLambdaLoop();
    };

    const clearCanvas = () => {
      ctx.fillStyle = "#000";
      ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
    };

    const drawGrid = () => {
      // ctx.globalAlpha = .2;
      // ctx.fillStyle = '#000';
      // ctx.clearRect(0,0,ctx.canvas.width,ctx.canvas.height);
      // let range = (equationValues.lambdaEnd-equationValues.lambdaStart);
      // //4
      // let pixelsPerArea = ctx.canvas.width/range;
      // //200 = 800/4
      // //400 = 800/2
      // console.log(pixelsPerArea);
      // console.log((equationValues.lambdaStart%range));
      // let offset = pixelsPerArea%(equationValues.lambdaStart*pixelsPerArea);
      // //0
      // //800
      // console.log(offset);
      // for(let i=0;i<range*pixelsPerArea;i+=pixelsPerArea) {
      //   ctx.fillRect(i, 0, 1, ctx.canvas.height);
      // }
      // // ctx.globalAlpha = .1;
      // // for(let i=1;i<8;i++) {
      // //   ctx.fillRect((ctx.canvas.width/8)*i, 0, 1, ctx.canvas.height);
      // //   ctx.fillRect(0, (ctx.canvas.height/8)*i, ctx.canvas.width, 1);
      // // }
      // // ctx.globalAlpha = .05;
      // // for(let i=1;i<32;i++) {
      // //   ctx.fillRect((ctx.canvas.width/32)*i, 0, 1, ctx.canvas.height);
      // //   ctx.fillRect(0, (ctx.canvas.height/32)*i, ctx.canvas.width, 1);
      // // }
    };

    const startLambdaLoop = () => {
      ctx.globalAlpha =
        (1 - (equationValues.superSamples - 10) / 50) * 0.005 +
        0.2 -
        ((equationValues.maxXValues - 100) / 900) * 0.1;
      ctx.fillStyle = "#fff";
      lambdaLoopsBeforeCanvasUpdate = 0;
      lambdaLoop();
    };

    const lambdaLoop = () => {
      if (lambda < equationValues.lambdaEnd) {
        iterations = 0;
        xValues = [];

        iterateMap(0.5);
        for (let i = 0; i < xValues.length; i++) {
          x =
            (lambda - equationValues.lambdaStart) *
            (ctx.canvas.width /
              (equationValues.lambdaEnd - equationValues.lambdaStart));
          y = -(xValues[i] * ctx.canvas.height) + ctx.canvas.height;
          ctx.fillRect(x, y, rectangleSize, rectangleSize);
        }
        lambda += equationValues.lambdaStep;
        if (lambdaLoopsBeforeCanvasUpdate > 200) {
          setTimeout(startLambdaLoop, 0);
        } else {
          lambdaLoopsBeforeCanvasUpdate++;
          lambdaLoop();
        }
      }
    };

    const iterateMap = x => {
      x = Math.round(100000 * (lambda * x * (1 - x))) / 100000;

      if (xValues.includes(x)) {
        return;
      }

      if (iterations < equationValues.minIterations) {
        iterations++;
        iterateMap(x);
      } else {
        xValues.push(x);
        if (xValues.length < equationValues.maxXValues) {
          iterateMap(x);
        }
      }
    };

    if (equationValues.committed) {
      draw();
    }

    return () => {
      lambda = equationValues.lambdaEnd;
    };
  }, [equationValues, clientDimensions]);

  return (
    <div className={classes.canvasContainer} ref={canvasContainer}>
      <canvas
        ref={canvas}
        width={clientDimensions[0]}
        height={clientDimensions[1]}
        style={{ width: "100%", height: "100%" }}
      />
    </div>
  );
}

export default Canvas;
