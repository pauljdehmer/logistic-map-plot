import AppBar from "@material-ui/core/AppBar";
import Button from "@material-ui/core/Button";
import Controls from "../Controls/Controls";
import { Divider } from "@material-ui/core";
import React from "react";
import SwipeableDrawer from "@material-ui/core/SwipeableDrawer";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import logisticMapSVG from "../../assets/images/logistic-map.svg";
import { makeStyles } from "@material-ui/core/styles";
import { useState } from "react";

const useStyles = makeStyles({
  list: {
    width: 350
  },
  content: {
    margin: "12px 25px"
  },
  button: {
    margin: "10px"
  }
});

function DrawerContainer() {
  const classes = useStyles();
  const [drawerOpen, setDrawerOpen] = useState(false);
  const iOS = process.browser && /iPad|iPhone|iPod/.test(navigator.userAgent);

  const toggleDrawer = open => event => {
    if (
      event &&
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }
    setDrawerOpen(open);
  };

  return (
    <div>
      <Button
        size="small"
        className={classes.button}
        variant="contained"
        onClick={toggleDrawer(true)}
      >
        Settings
      </Button>
      <SwipeableDrawer
        disableBackdropTransition={!iOS}
        disableDiscovery={iOS}
        open={drawerOpen}
        onOpen={toggleDrawer(true)}
        onClose={toggleDrawer(false)}
      >
        <div
          className={classes.list}
          role="presentation"
          onKeyDown={toggleDrawer(false)}
        >
          <AppBar position="static">
            <Toolbar>
              <Typography variant="h6" color="inherit">
                Logistic Map Plot
              </Typography>
            </Toolbar>
          </AppBar>
          <div className={classes.content}>
            <Typography variant="body2" component="div" gutterBottom>
              <p>
                Plots the{" "}
                <a
                  href="https://en.wikipedia.org/wiki/Bifurcation_diagram"
                  rel="noopener noreferrer"
                  target="_blank"
                >
                  bifurcation diagram
                </a>{" "}
                of the{" "}
                <a
                  href="https://en.wikipedia.org/wiki/Logistic_map"
                  rel="noopener noreferrer"
                  target="_blank"
                >
                  logistic map
                </a>
                :
              </p>
              <img src={logisticMapSVG} alt="logistic map" />
              <p>
                The horizontal axis shows the possible values of the parameter{" "}
                <em>a</em> while the vertical axis shows the set of values of{" "}
                <em>x</em> visited asymptotically from almost all initial
                conditions by the iterates of the logistic equation with that{" "}
                <em>a</em> value.
              </p>
              <p>The Sliders below allow you to modify:</p>
              <ul>
                <li>
                  <span>Lambda Range</span>
                  <ul>
                    <li>
                      Start and end values for <em>a</em>
                    </li>
                  </ul>
                </li>
                <li>
                  <span>
                    Max <em>x</em> Values
                  </span>
                  <ul>
                    <li>
                      Maximum of unique values of <em>x</em> at a given{" "}
                      <em>a</em>
                    </li>
                  </ul>
                </li>
                <li>
                  <span>Minimum Iteration Count</span>
                  <ul>
                    <li>
                      Number of iterations of the logistic map before including
                      the result as a value of <em>x</em> at that <em>a</em>
                    </li>
                  </ul>
                </li>
                <li>
                  <span>Super Samples</span>
                  <ul>
                    <li>
                      Multiply this number and the canvas width in pixels to
                      determine the number of steps of <em>a</em> in the lambda
                      range.
                    </li>
                  </ul>
                </li>
              </ul>
            </Typography>
            <Divider />
            <Controls />
          </div>
        </div>
      </SwipeableDrawer>
    </div>
  );
}

export default DrawerContainer;
