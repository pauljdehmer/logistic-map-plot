import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import { COMMIT_EQUATION_VALUES } from "../../redux/actionTypes";
import { RESET_EQUATION_VALUES } from "../../redux/actionTypes";
import React from "react";
import Slider from "@material-ui/core/Slider";
import { UPDATE_EQUATION_VALUES } from "../../redux/actionTypes";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";

function Controls() {
  const equationValuesRedux = useSelector(state => state.equationValues);
  const dispatch = useDispatch();
  let newState;

  const updateEquationValues = sliderName => (event, newValue) => {
    switch (sliderName) {
      case "lambdaRange":
        newState = {
          ...equationValuesRedux,
          lambdaStart: newValue[0],
          lambdaEnd: newValue[1],
          committed: false
        };
        break;
      case "maxXValues":
        newState = {
          ...equationValuesRedux,
          maxXValues: newValue,
          committed: false
        };
        break;
      case "minIterations":
        newState = {
          ...equationValuesRedux,
          minIterations: newValue,
          committed: false
        };
        break;
      case "superSamples":
        newState = {
          ...equationValuesRedux,
          superSamples: newValue,
          committed: false
        };
        break;
      default:
        break;
    }
    let equationValues = newState;
    dispatch({ type: UPDATE_EQUATION_VALUES, equationValues });
  };

  const commitEquationValues = () => {
    dispatch({ type: COMMIT_EQUATION_VALUES });
  };

  const resetValues = () => {
    dispatch({ type: RESET_EQUATION_VALUES });
  };

  return (
    <div className="slider-container">
      <Box>
        <p>Lambda Range</p>
        <Slider
          step={0.001}
          max={4}
          value={[
            equationValuesRedux.lambdaStart,
            equationValuesRedux.lambdaEnd
          ]}
          onChange={updateEquationValues("lambdaRange")}
          onChangeCommitted={commitEquationValues}
          valueLabelDisplay="auto"
          aria-labelledby="lambda-range-slider"
        />
      </Box>
      <Box>
        <p>Maximum X Values</p>
        <Slider
          step={1}
          min={100}
          max={1000}
          value={equationValuesRedux.maxXValues}
          onChange={updateEquationValues("maxXValues")}
          onChangeCommitted={commitEquationValues}
          valueLabelDisplay="auto"
          aria-labelledby="max-x-values-slider"
        />
      </Box>
      <Box>
        <p>Minimum Iteration Count</p>
        <Slider
          step={1}
          min={10}
          max={5000}
          value={equationValuesRedux.minIterations}
          onChange={updateEquationValues("minIterations")}
          onChangeCommitted={commitEquationValues}
          valueLabelDisplay="auto"
          aria-labelledby="min-iteration-count-slider"
        />
      </Box>
      <Box>
        <p>Super Samples</p>
        <Slider
          step={1}
          min={10}
          max={100}
          value={equationValuesRedux.superSamples}
          onChange={updateEquationValues("superSamples")}
          onChangeCommitted={commitEquationValues}
          valueLabelDisplay="auto"
          aria-labelledby="super-samples-slider"
        />
      </Box>
      <Box>
        <Button
          size="small"
          onClick={resetValues}
          variant="contained"
          color="primary"
        >
          Reset Values
        </Button>
      </Box>
    </div>
  );
}

export default Controls;
