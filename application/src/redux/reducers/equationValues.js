import { COMMIT_EQUATION_VALUES } from "../actionTypes";
import { RESET_EQUATION_VALUES } from "../actionTypes";
import { UPDATE_EQUATION_VALUES } from "../actionTypes";

const initialState = {
  lambdaStart: 0,
  lambdaEnd: 4,
  maxXValues: 500,
  minIterations: 1000,
  superSamples: 20,
  committed: true
};

export default function(state = initialState, action) {
  switch (action.type) {
    case UPDATE_EQUATION_VALUES:
      return action.equationValues;
    case COMMIT_EQUATION_VALUES:
      return {
        ...state,
        committed: true
      };
    case RESET_EQUATION_VALUES:
      return {
        ...initialState
      };
    default:
      return state;
  }
}
