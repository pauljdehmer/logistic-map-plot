import { combineReducers } from "redux";
import equationValues from "./equationValues";

export default combineReducers({
  equationValues
});
