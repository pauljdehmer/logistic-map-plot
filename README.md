[![pipeline status](https://gitlab.com/pauljdehmer/logistic-map-plot/badges/master/pipeline.svg)](https://gitlab.com/pauljdehmer/logistic-map-plot/-/commits/master)
[![coverage report](https://gitlab.com/pauljdehmer/logistic-map-plot/badges/master/coverage.svg)](https://gitlab.com/pauljdehmer/logistic-map-plot/-/commits/master)

# Logistic Map Plot

_Plots the [bifurcation diagram](https://en.wikipedia.org/wiki/Bifurcation_diagram)
of the [logistic map](https://en.wikipedia.org/wiki/Logistic_map)
f(x) = ax(1-x)._

## Dependencies

This project requires [Docker](https://www.docker.com/) to be installed.

## Usage

There are scripts included in the ```script``` directory of this project.

* ```bash.sh```
* ```build.sh```
* ```lint.sh```
* ```serve.sh```
* ```test.sh```

These scripts start or enter a docker container (if already running) with
the ```application``` folder mounted as a volume and execute commands on the
application. Run any of these with the ```--help``` option for more information
about them.

The ```gitlab-*``` scripts run tools and scanners provided by Gitlab on the
project. These are also run in the pipeline. Run any of these with the ```--help```
option for more information about them.

## Application

Plots the [bifurcation diagram](https://en.wikipedia.org/wiki/Bifurcation_diagram)
of the [logistic map](https://en.wikipedia.org/wiki/Logistic_map)
f(x) = ax(1-x).

## Demo

<https://pauljdehmer.gitlab.io/logistic-map-plot/>