#!/bin/bash

usage() {
  cat <<EOF
Usage: $0 [options]

Runs GitLab DAST on the application. Be sure to run the serve.sh script
with the -b option before running this script.

  -h, --help                 Display this help and exit.
  -u, --username             Username for logging into the site.
  -p, --password             Password for logging into the site.
EOF
}
getopt --test > /dev/null 
if [[ ${PIPESTATUS[0]} -ne 4 ]]; then
    echo 'I’m sorry, `getopt --test` failed in this environment.'
    exit 1
fi
OPTIONS=hu:p:
LONGOPTS=help,username:,password:
! PARSED=$(getopt --options=$OPTIONS --longoptions=$LONGOPTS --name "$0" -- "$@")
if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
    exit 2
fi
eval set -- "$PARSED"
h=n u=- p=-
while true; do
  case "$1" in
      -h|--help)
        usage
        exit 1
        ;;
      -u|--username)
        u="$2"
        shift 2
        ;;
      -p|--password)
        p="$2"
        shift 2
        ;;
      --)
        shift
        break
        ;;
      *)
        echo "Programming error"
        exit 3
        ;;
  esac
done

if [ $u = "-" ]; then
  echo "-u or --username option is required."
  exit 1
fi

if [ $p = "-" ]; then
  echo "-p or --password option is required."
  exit 1
fi

projectdir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"/../
cd $projectdir

if [ "$(docker ps -a | grep logistic-map-plot)" ]; then
  echo "Starting..."
else
  echo "Project not running. Have you started the server?"
  exit 1
fi

if [ -d dast ]; then
  docker run \
    -it \
    --rm \
    -v $projectdir:/project \
    -w /project \
    alpine \
    /bin/sh -c "rm -r dast"
fi

docker run \
  -it \
  --rm \
  --net=host \
  -e DAST_FULL_SCAN_ENABLED=true \
  -v $projectdir/dast:/output:rw \
  -v $projectdir/dast:/zap/wrk:rw \
  registry.gitlab.com/gitlab-org/security-products/dast /analyze \
    -t http://localhost:3000 \
    --auth-url http://localhost:3000/login \
    --auth-username $u \
    --auth-password $p \
    --auth-username-field username \
    --auth-password-field password \
    -r report.html
