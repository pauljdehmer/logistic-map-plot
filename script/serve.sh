#!/bin/bash

usage() {
  cat <<EOF
Usage: $0 [options]

Starts the project Docker container and runs the create-react-app dev
server.

  -h, --help                 Display this help and exit.
  -b, --build                If provided, creates a production build
                             and serves it using the npm package, serve.
  -e, --environment          The backend environment to target. i.e. dev,
                             staging, or master. Defaults to dev.
EOF
}
getopt --test > /dev/null 
if [[ ${PIPESTATUS[0]} -ne 4 ]]; then
    echo 'I’m sorry, `getopt --test` failed in this environment.'
    exit 1
fi
OPTIONS=be:h
LONGOPTS=build,environment:,help
! PARSED=$(getopt --options=$OPTIONS --longoptions=$LONGOPTS --name "$0" -- "$@")
if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
    exit 2
fi
eval set -- "$PARSED"
b=n e=- h=n
while true; do
  case "$1" in
      -h|--help)
        usage
        exit 1
        ;;
      -b|--build)
        b=y
        shift
        ;;
      -e|--environment)
        e="$2"
        shift 2
        ;;
      --)
        shift
        break
        ;;
      *)
        echo "Programming error"
        exit 3
        ;;
  esac
done

if [ $e = "-" ]; then
  e=dev
fi

if [ $e != 'local' ] && [ $e != 'dev' ] && [ $e != 'staging' ] && [ $e != 'master' ]; then
  echo "Environment must be 'local', 'dev', 'staging', or 'master'."
  exit 3
fi

projectdir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"/../
cd $projectdir

if [ "$(docker ps -a | grep logistic-map-plot)" ]; then
  docker kill logistic-map-plot
fi

if [ "$b" = "y" ]; then
  command="npm run build && npx serve -s -l 3000 build"
else
  command="npm run start"
fi

docker run \
  -it \
  --rm \
  --name logistic-map-plot \
  --net=host \
  -v $projectdir/application:/application \
  -e REACT_APP_ENVIRONMENT=$e \
  -e EXTEND_ESLINT=true \
  -w /application \
  node \
  /bin/bash -c \
    "npm ci \
    && $command"
