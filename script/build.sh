#!/bin/bash

usage() {
  cat <<EOF
Usage: $0 [options]

Starts the project Docker container and runs the react build script.

  -h, --help                 Display this help and exit.
  -e, --environment          The backend environment to target. i.e. dev,
                             staging, or master. Defaults to dev.
EOF
}
getopt --test > /dev/null 
if [[ ${PIPESTATUS[0]} -ne 4 ]]; then
    echo 'I’m sorry, `getopt --test` failed in this environment.'
    exit 1
fi
OPTIONS=e:h
LONGOPTS=environment:,help
! PARSED=$(getopt --options=$OPTIONS --longoptions=$LONGOPTS --name "$0" -- "$@")
if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
    exit 2
fi
eval set -- "$PARSED"
e=- h=n
while true; do
  case "$1" in
      -h|--help)
        usage
        exit 1
        ;;
      -e|--environment)
        e="$2"
        shift 2
        ;;
      --)
        shift
        break
        ;;
      *)
        echo "Programming error"
        exit 3
        ;;
  esac
done

if [ $e = "-" ]; then
  e=dev
fi

if [ $e != 'local' ] && [ $e != 'dev' ] && [ $e != 'staging' ] && [ $e != 'master' ]; then
  echo "Environment must be 'local', 'dev', 'staging', or 'master'."
  exit 3
fi

projectdir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"/../
cd $projectdir

if [ "$(docker ps -a | grep logistic-map-plot)" ]; then
  docker kill logistic-map-plot
fi

if [ -d application/build ]; then
  docker run \
    -it \
    --rm \
    -v $projectdir:/project \
    -w /project/application \
    alpine \
    /bin/sh -c "rm -r build"
fi

docker run \
  -it \
  --rm \
  --name logistic-map-plot \
  -v $projectdir/application:/application \
  -e REACT_APP_ENVIRONMENT=$e \
  -e EXTEND_ESLINT=true \
  -w /application \
  node \
  /bin/bash -c \
    "npm ci \
    && npm run build"
